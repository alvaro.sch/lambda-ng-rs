# Lambda NG rs

Very simple repl that parses lambda expressions and applies reduction once, it also prints the free variables of the expression after the reduction.

It features very basic error reporting.

## Syntax

```
<expr> ::= <var>
         | <expr> <expr>
         | '(' <expr> ')'
         | ('\' | 'λ') <var>+ '.' <expr>
```

### Examples

A free variable
```
x
```

A nested lambda function returning a free variable z
```
\x.\y.z
```

Multiple variables are treated nested lambdas as well, making the following equivalent to the previous example
```
\x y.z
```

A lambda function being applied some variable
```
(\x.x) y
```

A lambda function being applied to itself, the reduction returns the same expression
```
(\x.x x)\x.x x
```
