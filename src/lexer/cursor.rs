#[derive(Debug, Clone)]
pub struct Cursor<'a> {
    pub slice: &'a str,
    pos: usize,
    col: usize,
}

impl<'a> Cursor<'a> {
    pub fn new(slice: &'a str) -> Self {
        Self {
            slice,
            pos: 0,
            col: 0,
        }
    }

    pub fn pos(&self) -> usize {
        self.pos
    }

    pub fn col(&self) -> usize {
        self.col
    }

    fn next_codepoint(&self, look_ahead: usize) -> usize {
        let offset = self.pos + look_ahead;
        let mut code_len = 1;
        while !self.slice.is_char_boundary(offset + code_len)
            && offset + code_len < self.slice.len()
        {
            code_len += 1;
        }

        code_len
    }

    pub fn peek(&self) -> Option<char> {
        let code_start = self.pos;
        let code_end = code_start + self.next_codepoint(0);

        self.slice
            .get(code_start..code_end)
            .and_then(|s| s.chars().next())
    }

    #[allow(unused)]
    pub fn peek_next(&self) -> Option<char> {
        let prev_code_len = self.next_codepoint(0);
        let code_start = self.pos + prev_code_len;
        let code_end = code_start + self.next_codepoint(prev_code_len);

        self.slice
            .get(code_start..code_end)
            .and_then(|s| s.chars().next())
    }

    pub fn next(&mut self) -> Option<char> {
        self.peek().map(|tok| {
            let len = tok.len_utf8();

            self.col += 1;
            self.pos += len;

            tok
        })
    }
}
