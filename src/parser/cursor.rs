use crate::{Error, Token, TokenKind};

#[derive(Debug, Clone)]
pub struct Cursor<'a> {
    pub ts: &'a [Token<'a>],
    pos: usize,
}

impl<'a> Cursor<'a> {
    pub fn new(ts: &'a [Token]) -> Self {
        Self { ts, pos: 0 }
    }

    pub fn peek(&self) -> Option<&Token> {
        self.ts.get(self.pos)
    }

    pub fn accept<'b>(&'b mut self, kind: TokenKind) -> Option<&'b Token<'a>>
    where
        'a: 'b,
    {
        match self.ts.get(self.pos) {
            Some(tok) if kind == tok.kind => {
                self.pos += 1;
                Some(tok)
            }

            _ => None,
        }
    }

    pub fn expect<'b>(&'b mut self, kind: TokenKind) -> Result<&'b Token<'a>, Error>
    where
        'a: 'b,
    {
        match self.ts.get(self.pos) {
            Some(tok) if kind == tok.kind => {
                self.pos += 1;
                Ok(tok)
            }

            Some(tok) if tok.kind == TokenKind::EOL => {
                let expected = format!("{}", kind);
                Err(Error::unexpected_eol(expected, tok.position.start))
            }

            Some(tok) => Err(Error::unexpected_token(kind, tok)),

            None => unreachable!(),
        }
    }
}
