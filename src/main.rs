use lambda_ng::{
    error, lexer, parser,
    repl::{Line, Repl},
};

type ProcessResult = Result<(), i32>;

fn main() -> ProcessResult {
    let mut last_result = Ok(());

    let _ = Repl::default().run(|repl, line| match line {
        Line::Empty => {}

        Line::Command("exit", _) => repl.quit(),

        Line::Command("prompt", &[prompt, ..]) => repl.set_prompt(format!("{prompt} > ")),

        Line::Command(name, _) => {
            eprintln!("command {name} is invalid or doesn't have enough arguments")
        }

        Line::Expression(expr) => last_result = eval_expression(expr),
    });

    last_result
}

fn eval_expression(expr: &str) -> ProcessResult {
    let ts = lexer::tokenize(expr);

    let ast = match parser::parse_expression(&ts) {
        Ok(ast) => ast,
        Err(errors) => {
            error::report(expr, &errors);
            return Err(1);
        }
    };

    let reduced = ast.reduce();
    let fvs = reduced.free_variables();

    println!("out >");
    println!(" (orig) {}", ast.colorized_display());
    println!(" (red)  {}", reduced.colorized_display());
    println!(" (fvs)  {fvs:?}");

    Ok(())
}
