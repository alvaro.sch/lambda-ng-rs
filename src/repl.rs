mod command;

use rustyline::{
    error::ReadlineError, highlight::MatchingBracketHighlighter, Completer, Helper, Highlighter,
    Hinter, Validator,
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Line<'a> {
    Empty,

    Expression(&'a str),

    Command(&'a str, &'a [&'a str]),
}

#[derive(Helper, Hinter, Completer, Validator, Highlighter)]
pub struct ParentHightlighter {
    #[rustyline(Highlighter)]
    hightlighter: MatchingBracketHighlighter,
}

impl ParentHightlighter {
    pub fn new() -> Self {
        Self {
            hightlighter: MatchingBracketHighlighter::new(),
        }
    }
}

pub struct Repl {
    prompt: String,
    should_quit: bool,
}

impl Default for Repl {
    fn default() -> Self {
        Self {
            prompt: "repl > ".to_string(),
            should_quit: false,
        }
    }
}

pub type ReplResult<T> = Result<T, ReadlineError>;

impl Repl {
    pub fn with_prompt(mut self, prompt: impl Into<String>) -> Self {
        self.prompt = prompt.into();
        self
    }
}

impl Repl {
    pub fn quit(&mut self) {
        self.should_quit = true;
    }

    pub fn set_prompt(&mut self, prompt: impl Into<String>) {
        self.prompt = prompt.into();
    }

    pub fn run<F>(&mut self, mut update: F) -> ReplResult<()>
    where
        F: FnMut(&mut Self, Line),
    {
        let config = rustyline::Config::builder()
            .auto_add_history(true)
            .history_ignore_space(true)
            .build();

        let mut rl = rustyline::Editor::with_config(config)?;
        rl.set_helper(Some(ParentHightlighter::new()));

        let _ = rl.load_history(".lnghist");

        while !self.should_quit {
            let raw_line = match rl.readline(&self.prompt) {
                Ok(line) => line,
                Err(ReadlineError::Interrupted | ReadlineError::Eof) => break,
                Err(e) => return Err(e),
            };

            let raw_line = raw_line.trim();
            let mut args = Vec::new();

            let line = match command::parse(raw_line, &mut args) {
                Some((name, args)) => Line::Command(name, args),

                None if raw_line.is_empty() => Line::Empty,

                None => Line::Expression(raw_line),
            };

            update(self, line);
        }

        let _ = rl.save_history(".lnghist");

        Ok(())
    }
}
