use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use owo_colors::{AnsiColors, OwoColorize};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Expression {
    Variable {
        identifier: String,
    },
    Application {
        operator: Box<Expression>,
        operand: Box<Expression>,
    },
    Abstraction {
        variable: String,
        expression: Box<Expression>,
    },
}

pub type Set = HashSet<String>;
pub type Map = HashMap<String, Box<Expression>>;

impl Expression {
    fn boxed_clone(&self) -> Box<Self> {
        Box::new(self.clone())
    }

    pub fn variable(identifier: impl Into<String>) -> Box<Self> {
        Box::new(Self::Variable {
            identifier: identifier.into(),
        })
    }

    pub fn application(operator: Box<Self>, operand: Box<Self>) -> Box<Self> {
        Box::new(Self::Application { operator, operand })
    }

    pub fn abstraction(variable: impl Into<String>, expression: Box<Self>) -> Box<Self> {
        Box::new(Self::Abstraction {
            variable: variable.into(),
            expression,
        })
    }

    pub fn free_variables(&self) -> Set {
        let mut free = Set::new();
        let mut bounded = Set::new();

        self._free_vars(&mut free, &mut bounded);

        free
    }

    pub fn swap(&self, delta: &mut Map) -> Box<Self> {
        match self {
            Self::Variable { identifier } => Self::_replace_or_default(delta, identifier),

            Self::Application { operator, operand } => {
                Self::application(operator.swap(delta), operand.swap(delta))
            }

            Self::Abstraction {
                variable,
                expression,
            } => {
                let new_var = expression._swap_var(variable, delta);
                delta.insert(variable.clone(), Self::variable(new_var.clone()));

                Self::abstraction(new_var, expression.swap(delta))
            }
        }
    }

    pub fn reduce(&self) -> Box<Self> {
        match self {
            Self::Variable { identifier } => Self::variable(identifier),

            Self::Application { operator, operand } => {
                if let Self::Abstraction {
                    variable,
                    expression,
                } = operator.as_ref()
                {
                    let mut map = Map::new();
                    map.insert(variable.clone(), operand.clone());

                    expression.swap(&mut map)
                } else {
                    self.boxed_clone()
                }
            }

            Self::Abstraction {
                variable,
                expression,
            } => Self::abstraction(variable, expression.reduce()),
        }
    }
}

impl Expression {
    fn _replace_or_default(delta: &mut Map, ident: &str) -> Box<Self> {
        delta
            .get(ident)
            .map(|expr| expr.boxed_clone())
            .unwrap_or_else(|| Self::variable(ident))
    }

    fn _free_vars(&self, free: &mut HashSet<String>, bounded: &mut HashSet<String>) {
        match self {
            Self::Variable { identifier } if !bounded.contains(identifier) => {
                free.insert(identifier.to_owned());
            }

            Self::Application { operator, operand } => {
                let operator_bounded = &mut bounded.clone();
                let operand_bounded = bounded;

                operator._free_vars(free, operator_bounded);
                operand._free_vars(free, operand_bounded);
            }

            Self::Abstraction {
                variable,
                expression,
            } => {
                bounded.insert(variable.to_owned());
                expression._free_vars(free, bounded);
            }

            _ => {}
        }
    }

    fn _swap_var(&self, var: &str, delta: &mut Map) -> String {
        let mut replaced_variables = self.free_variables();
        replaced_variables.remove(var);

        let replaced_variables = replaced_variables
            .into_iter()
            .flat_map(|ident| Self::_replace_or_default(delta, &ident).free_variables())
            .collect::<Set>();

        let mut new_var = var.to_string();
        while replaced_variables.contains(&new_var) {
            new_var.push('\'');
        }

        new_var
    }
}

impl Display for Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Variable { identifier } => write!(f, "{identifier}"),

            Self::Application { operator, operand } => write!(f, "({operator} {operand})"),

            Self::Abstraction {
                variable,
                expression,
            } => write!(f, "λ {variable} . {expression}"),
        }
    }
}

impl Expression {
    pub fn colorized_display(&self) -> ColorizedDisplay {
        ColorizedDisplay {
            expression: self,
            depth: 0,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ColorizedDisplay<'a> {
    expression: &'a Expression,
    depth: usize,
}

impl<'a> ColorizedDisplay<'a> {
    const COLOR_LUT: [AnsiColors; 8] = [
        AnsiColors::BrightRed,
        AnsiColors::BrightGreen,
        AnsiColors::BrightYellow,
        AnsiColors::BrightBlue,
        AnsiColors::BrightMagenta,
        AnsiColors::BrightCyan,
        AnsiColors::BrightWhite,
        AnsiColors::BrightBlack,
    ];

    fn get_color(depth: usize) -> AnsiColors {
        Self::COLOR_LUT[depth % 8]
    }

    fn next_depth(&self, expression: &'a Expression) -> Self {
        Self {
            expression,
            depth: self.depth + 1,
        }
    }
}

impl<'a> Display for ColorizedDisplay<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.expression {
            Expression::Variable { identifier } => write!(f, "{identifier}"),

            Expression::Application { operator, operand } => {
                let color = ColorizedDisplay::get_color(self.depth);

                let lparen = "(".color(color);
                let rparen = ")".color(color);

                let operator = self.next_depth(operator);
                let operand = self.next_depth(operand);

                write!(f, "{lparen}{operator} {operand}{rparen}")
            }

            Expression::Abstraction {
                variable,
                expression,
            } => write!(f, "λ {variable} . {}", self.next_depth(expression)),
        }
    }
}
