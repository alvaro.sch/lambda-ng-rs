mod cursor;

use std::ops::Range;

use cursor::Cursor;
use derive_more::Display;

#[derive(Debug, Display, Clone, Copy, PartialEq, Eq)]
pub enum TokenKind {
    #[display(fmt = ".")]
    Dot, // .

    #[display(fmt = "=")]
    Equal, // =

    #[display(fmt = "\\ or λ")]
    Lambda, // \ λ

    #[display(fmt = "(")]
    LParen, // (

    #[display(fmt = ")")]
    RParen, // )

    #[display(fmt = "<variable>")]
    Variable, // any sequence not containing the symbols above

    #[display(fmt = "\\n")]
    EOL,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TokenData<'a> {
    Identifier(&'a str),
}

impl<'a> TokenData<'a> {
    pub fn identifier(&self) -> &str {
        match self {
            Self::Identifier(s) => s,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Token<'a> {
    pub kind: TokenKind,
    pub data: Option<TokenData<'a>>,
    pub position: Range<usize>,
}

fn is_ident_separator(c: char) -> bool {
    " \t.=\\λ()".contains(c)
}

impl<'a> Token<'a> {
    fn symbol(kind: TokenKind, cursor: &Cursor) -> Self {
        let start = cursor.col() - 1;
        let end = cursor.col();

        Self {
            kind,
            data: None,
            position: start..end,
        }
    }

    fn identifier(fst_char: char, cursor: &mut Cursor<'a>) -> Self {
        let start = cursor.col() - 1;
        let cursor_start_pos = cursor.pos() - fst_char.len_utf8();

        while let Some(sym) = cursor.peek() {
            if is_ident_separator(sym) {
                break;
            }
            cursor.next();
        }

        let cursor_end_pos = cursor.pos();
        let chunk = cursor.slice.get(cursor_start_pos..cursor_end_pos).unwrap();

        let end = cursor.col();

        Self {
            kind: TokenKind::Variable,
            data: Some(TokenData::Identifier(chunk)),
            position: start..end,
        }
    }

    fn eol(cursor: &Cursor) -> Self {
        Self {
            kind: TokenKind::EOL,
            data: None,
            position: cursor.col()..cursor.col() + 1,
        }
    }

    pub(crate) fn data_as_variable(&self) -> &str {
        self.data.as_ref().unwrap().identifier()
    }
}

pub type TokenStream<'a> = Vec<Token<'a>>;

pub fn tokenize(expr: &str) -> TokenStream {
    let mut ts = Vec::new();
    let mut cursor = Cursor::new(expr);

    while let Some(sym) = cursor.next() {
        match sym {
            c if c.is_whitespace() => {}

            '.' => ts.push(Token::symbol(TokenKind::Dot, &cursor)),

            '=' => ts.push(Token::symbol(TokenKind::Equal, &cursor)),

            '(' => ts.push(Token::symbol(TokenKind::LParen, &cursor)),

            ')' => ts.push(Token::symbol(TokenKind::RParen, &cursor)),

            '\\' | 'λ' => ts.push(Token::symbol(TokenKind::Lambda, &cursor)),

            c => ts.push(Token::identifier(c, &mut cursor)),
        }
    }

    ts.push(Token::eol(&cursor));

    ts
}
