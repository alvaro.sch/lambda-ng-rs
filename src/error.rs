use std::ops::Range;

use crate::{Token, TokenKind};
use derive_more::{Display, Error};

#[derive(Debug, Display, Clone, Error, PartialEq)]
pub enum ErrorKind {
    #[display(fmt = "unexpected token '{got}' [expected '{expected}']")]
    UnexpectedToken { expected: TokenKind, got: TokenKind },

    #[display(fmt = "leading tokens after valid expression")]
    LeadingTokens {},

    #[display(fmt = "unexpected token '{got}' [expected any of '\\ λ ( <var>']")]
    InvalidSOE { got: TokenKind },

    #[display(fmt = "unexpected end of line during expression [expected '{expected}']")]
    UnexpectedEOL { expected: String },
}

#[derive(Debug, Clone, PartialEq)]
pub struct Error {
    pub position: Range<usize>,
    pub kind: ErrorKind,
}

impl Error {
    pub fn unexpected_token(expected: TokenKind, got: &Token) -> Self {
        Self {
            position: got.position.clone(),
            kind: ErrorKind::UnexpectedToken {
                expected,
                got: got.kind,
            },
        }
    }

    pub fn invalid_soe(got: &Token) -> Self {
        Self {
            position: got.position.clone(),
            kind: ErrorKind::InvalidSOE { got: got.kind },
        }
    }

    pub fn unexpected_eol(expected: impl Into<String>, col: usize) -> Self {
        Self {
            position: col..col + 1,
            kind: ErrorKind::UnexpectedEOL {
                expected: expected.into(),
            },
        }
    }
}

pub fn report(source: &str, errors: &[Error]) {
    for error in errors {
        eprintln!("error near column {}", error.position.start);
        eprintln!("\t{source}");

        let space_pad = " ".repeat(error.position.start);
        let arrow_pad = "^".repeat(error.position.len());

        eprintln!("\t{space_pad}{arrow_pad} {}", error.kind);
    }
}
