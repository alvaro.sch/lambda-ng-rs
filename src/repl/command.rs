pub fn parse<'a, 'b>(
    command: &'a str,
    args: &'b mut Vec<&'a str>,
) -> Option<(&'a str, &'b [&'a str])> {
    if !command.starts_with(':') {
        return None;
    }

    args.clear();

    let command = &command[1..];

    let mut it = command.split(&[' ', '\t']);
    let name = it.next()?;

    for arg in it {
        args.push(arg);
    }

    Some((name, args))
}
