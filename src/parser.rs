mod cursor;

use crate::{Error, Expression, TokenKind, TokenStream};
use cursor::Cursor;

/*
 * <expr> ::= <expr'>+
 *
 * <expr'> ::= <var>
 *           | <lambda> <var>+ '.' <expr>
 *           | '(' <expr> ')'
 */

type Expr = Option<Box<Expression>>;

fn root(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    let root = expr(cursor, errors)?;

    cursor
        .expect(TokenKind::EOL)
        .map_err(|error| errors.push(error))
        .ok()?;

    Some(root)
}

fn expr(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    seq(cursor, errors)
}

fn seq(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    let mut exprs = Vec::new();
    while let Some(expr) = expr2(cursor, errors) {
        exprs.push(expr);
    }

    let leaf = exprs.first()?.clone();
    let expr = exprs
        .into_iter()
        .skip(1)
        .fold(leaf, Expression::application);

    Some(expr)
}

fn expr2(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    let tok = cursor.peek()?;

    match tok.kind {
        TokenKind::Variable => var(cursor, errors),

        TokenKind::LParen => parens(cursor, errors),

        TokenKind::Lambda => abstraction(cursor, errors),

        _ => {
            errors.push(Error::invalid_soe(tok));
            None
        }
    }
}

fn var(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    let tok = cursor
        .expect(TokenKind::Variable)
        .map_err(|error| errors.push(error))
        .ok()?;

    Some(Expression::variable(tok.data_as_variable()))
}

fn parens(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    cursor
        .expect(TokenKind::LParen)
        .map_err(|error| errors.push(error))
        .ok()?;

    let expr = expr(cursor, errors)?;

    cursor
        .expect(TokenKind::RParen)
        .map_err(|error| errors.push(error))
        .ok()?;

    Some(expr)
}

fn abstraction(cursor: &mut Cursor, errors: &mut Vec<Error>) -> Expr {
    cursor
        .expect(TokenKind::Lambda)
        .map_err(|error| errors.push(error))
        .ok()?;

    let variable = cursor
        .expect(TokenKind::Variable)
        .map_err(|error| errors.push(error))
        .ok()?
        .data_as_variable()
        .to_string();

    let mut extra_vars = Vec::new();
    while let Some(var) = cursor.accept(TokenKind::Variable) {
        extra_vars.push(var.data_as_variable().to_string());
    }

    cursor
        .expect(TokenKind::Dot)
        .map_err(|error| errors.push(error))
        .ok()?;

    let innermost_expression = expr(cursor, errors)?;
    let expr = extra_vars
        .into_iter()
        .rfold(innermost_expression, |expr, var| {
            Expression::abstraction(var, expr)
        });

    Some(Expression::abstraction(variable, expr))
}

pub type ParseResult = Result<Box<Expression>, Vec<Error>>;

pub fn parse_expression<'a>(stream: &'a TokenStream<'a>) -> ParseResult {
    let mut cursor = Cursor::new(stream);
    let mut errors = Vec::new();

    root(&mut cursor, &mut errors).ok_or(errors)
}
