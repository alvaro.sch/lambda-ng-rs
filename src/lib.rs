pub mod error;
pub mod expression;
pub mod lexer;
pub mod parser;
pub mod repl;

pub use self::{
    error::{Error, ErrorKind},
    expression::Expression,
    lexer::{Token, TokenData, TokenKind, TokenStream},
    repl::Repl,
};

/*
 * <expr> ::= <var>
 *          | <expr> <expr>
 *          | '(' <expr> ')'
 *          | ('\' | 'λ') <var>+ '.' <expr>
 */
